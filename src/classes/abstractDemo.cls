/*
    Abstract class may or may not include abstract methods. 
    Abstract classes cannot be instantiated and they may contain methods declared with or without an implementation.
*/
        
public abstract class abstractDemo{

    /*
    An abstract method is a method that is declared without an implementation (without braces, and followed by a semicolon)
    If a class includes abstract methods, then the class itself must be declared abstract
    
    When an abstract class is subclassed, the subclass usually provides implementations for all of the abstract methods in its parent class. 
    However, if it does not, then the subclass must also be declared abstract.
    */
    
    integer x, y;
    
    void moveTo(integer newX, integer newY) {
        //code here
    }
    
    abstract void moveTo(double deltaX, double deltaY);
    
}